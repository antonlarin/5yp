.PHONY: all clean texfirst texsecond builddir refs

IMAGES = images/tfsf-boundary.pdf images/yee-cell.pdf \
	 images/fdtd-leapfrog.pdf images/mr-diagram.pdf \
	 images/mr-auxgrid-diagram.pdf images/sgdo-diagram.pdf

builddir:
	mkdir -p build

images/%.pdf : images/%.asy
	cd images && asy -f pdf $(notdir $<) && cd ..

texfirst: ${IMAGES} | builddir
	xelatex -output-directory=build 5yp.tex

refs: texfirst
	biber build/5yp.bcf

texsecond: refs
	xelatex -output-directory=build 5yp.tex
	
clean:
	rm -r build/
	rm -r images/*.pdf

all: texsecond

