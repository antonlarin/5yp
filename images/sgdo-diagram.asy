import common;

unitsize(0.9cm);

// pmls
fill((8, 9)--(10.3, 9)--(10.3, 10)--(8, 10)--cycle, mediumgray);
fill((7, 3)--(10.3, 3)--(10.3, 4)--(7, 4)--cycle, mediumgray);
fill((-0.3, 0)--(4, 0)--(4, 1)--(-0.3, 1)--cycle, mediumgray);

// coarse grid
draw((-0.3,9)--(10.3,9));
for (int i = 2; i <= 8; i += 3)
    bfieldarrow((i, 9));
for (int i = 0; i <= 9; i += 3)
    efieldcross((i + 0.5, 9));

// aux coarse grid
draw((-0.3, 6)--(5, 6));
for (int i = 2; i <= 5; i += 3)
{
    bfieldarrow((i, 6));
    efieldcross((i-1.5, 6));
}

// aux fine grid
draw((5, 3)--(10.3, 3));
for (int i = 5; i <= 10; i += 1)
    bfieldarrow((i, 3));
for (int i = 5; i <= 9; i += 1)
    efieldcross((i+0.5, 3));

// fine grid
draw((-0.3, 0)--(10.3, 0));
for (int i = 0; i <= 10; i += 1)
    bfieldarrow((i, 0));
for (int i =0; i <= 9; i += 1)
    efieldcross((i+0.5, 0));

label("Крупная", (-0.5, 9), W);
label("Вспомог. крупная", (-0.5, 6), W);
label("Вспомог. мелкая", (10.5, 3), E);
label("Мелкая", (10.5, 0), E);

// transfers
pen tr = dashdotted + linewidth(0.8pt);
arrowbar trarr = Arrow(HookHead, 7bp);
draw((5.1, 8.9)..(6.1, 6)..(5.1, 3.1), arrow=trarr, p=tr);
draw((4.9, 0.1)..(3.9, 3)..(4.9, 5.9), arrow=trarr, p=tr);

tr = dashed + linewidth(0.8pt);
draw((5, 2.9)..(5.5, 0.1), p=tr, arrow=trarr);
draw((5.5, 2.9)..(5.1, 0.1), p=tr, arrow=trarr);
draw((4.9, 6.1)..(3.5, 8.9), p=tr, arrow=trarr);
draw((3.5, 6.1)..(5, 8.9), p=tr, arrow=trarr);

// transfer actors markings
label("$\left.E_y^c\right|_{i-\frac12}$", (3.5, 9.1), N);
label("$\left.B_z^c\right|_i$", (5, 10.1), N);

label("$\left.B_z^f\right|_j$", (5, -0.1), SW);
label("$\left.E_y^f\right|_{j+\frac12}$", (5.5, -0.1), SE);

label("$\left.E_y^{ac}\right|_{k-\frac12}$", (3.5, 5.9), S);
label("$\left.B_z^{ac}\right|_k$", (4.8, 5.9), SE);

label("$\left.B_z^{af}\right|_l$", (5.1, 3), W);
label("$\left.E_y^{af}\right|_{l+\frac12}$", (5.5, 2.9), SE);
