import common;

unitsize(0.9cm);

// pmls
fill((13, 8)--(14.3, 8)--(14.3, 9)--(13, 9)--cycle, mediumgray);
fill((-0.3, 4)--(3, 4)--(3, 5)--(-0.3, 5)--cycle, mediumgray);
fill((11, 4)--(14.3, 4)--(14.3, 5)--(11, 5)--cycle, mediumgray);
fill((-0.3, 0)--(6, 0)--(6, 1)--(-0.3, 1)--cycle, mediumgray);

// coarse grid
draw((-0.3,8)--(14.3,8));
for (int i = 1; i <= 13; i += 3)
    bfieldarrow((i, 8));
for (int i = 2; i <= 11; i += 3)
    efieldcross((i + 0.5, 8));

// aux grid
draw((-0.3, 4)--(14.3, 4));
for (int i = 0; i <= 14; i += 1)
    bfieldarrow((i, 4));
for (int i = 0; i <= 13; i += 1)
    efieldcross((i+0.5, 4));

// fine grid
draw((-0.3, 0)--(14.3, 0));
for (int i = 0; i <= 14; i += 1)
    bfieldarrow((i, 0));
for (int i = 0; i <= 13; i += 1)
    efieldcross((i+0.5, 0));

label("Крупная", (-0.5, 8), W);
label("Вспомогательная", (-0.5, 4), W);
label("Мелкая", (-0.5, 0), W);

// transfers
pen tr = dashdotted + linewidth(0.8pt);
arrowbar trarr = Arrow(HookHead, 7bp);
draw((10, 3.9)..(9.5, 0.1), arrow=trarr, p=tr);
draw((9.5, 3.9)..(9.9, 0.1), arrow=trarr, p=tr);

draw((4, 7.9)..(3.5, 4.1), arrow=trarr, p=tr);
draw((2.5, 7.9)..(3.9, 5.2), arrow=trarr, p=tr);
label("$\frac23$", (2.7, 7), W);
draw((5.5, 7.9)..(4.1, 5.2), arrow=trarr, p=tr);
label("$\frac13$", (5.3, 7), E);

tr = dashed + linewidth(0.8pt);
draw((7.1, 0.1)..(8.5, 7.9), p=tr, arrow=trarr);
draw((8.5, 0.1)..(7, 7.9), p=tr, arrow=trarr);

// transfer actors markers
label("$\left.E_y^c\right|_{i-\frac12}$", (2.5, 8.1), N);
label("$\left.B_z^c\right|_i$", (4, 9.1), N);
label("$\left.E_y^c\right|_{i+\frac12}$", (5.5, 8.1), N);

label("$\left.E_y^a\right|_{k-\frac12}$", (3.5, 3.9), S);
label("$\left.B_z^a\right|_k$", (3.9, 4.7), E);
label("$\left.B_z^a\right|_{k+6}$", (10, 3.9), SE);

label("$\left.B_z^f\right|_{j+6}$", (9.9, 0.7), E);
label("$\left.E_y^f\right|_{j+\frac{11}{2}}$", (9.5, -0.1), S);

