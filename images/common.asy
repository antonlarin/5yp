import unicode;
texpreamble("\usepackage{mathtext}\usepackage[russian]{babel}");
defaultpen(font("T2A","cmr","m","n"));

void bfieldarrow(pair base)
{
    pair top = base + (0, 1);
    draw(base--top, arrow=Arrow(TeXHead));
}

void efieldcross(pair center)
{
    real halfsize = 0.1;
    pair nw = center + (-halfsize, halfsize);
    pair ne = center + (halfsize, halfsize);
    pair sw = center - (halfsize, halfsize);
    pair se = center - (-halfsize, halfsize);
    draw(nw--se);
    draw(sw--ne);
}

