import common;

unitsize(0.9cm);

// grid
draw((0,2)--(16,2), arrow=Arrow(SimpleHead));
for (int i = 1; i <= 13; i += 4)
{
    bfieldarrow((i, 2));
    efieldcross((i + 2, 2));
}
draw((9, 3)--(9, 4), p=dashed);
draw((9, 2)--(9, 1), p=dashed);
label("$x$", (16, 2), E);
label("$\left.B_z\right|_i$", (9.5, 3.5));
label("$\left.E_y\right|_{i-\frac12}$", (7, 2.7));
label("$\left(i-2\right)\Delta x$", (1, 1.6));
label("$\left(i-1\right)\Delta x$", (5, 1.6));
label("$i\Delta x$", (9.2, 1.6));
label("$\left(i+1\right)\Delta x$", (13, 1.6));

label("Total-field область", (13, 4), p=fontsize(14));
label("Scattered-field область", (5, 4), p=fontsize(14));
