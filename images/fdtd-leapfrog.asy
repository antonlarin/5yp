unitsize(1cm);

// axis
draw((1, 0)--(19, 0), arrow=Arrow(HookHead));
label("$t$", (19, 0), E);

// ticks
draw((2, -0.1)--(2, 0.1));
label("$n\Delta t$", (2, 0.1), N);

draw((5, -0.1)--(5, 0.1));
label("$\left(n+\frac12\right)\Delta t$", (5, -0.1), S);

draw((8, -0.1)--(8, 0.1));
label("$\left(n+1\right)\Delta t$", (8, 0.1), N);

draw((11, -0.1)--(11, 0.1));
label("$\left(n+\frac32\right)\Delta t$", (11, -0.1), S);

draw((14, -0.1)--(14, 0.1));
label("$\left(n+2\right)\Delta t$", (14, 0.1), N);

draw((17, -0.1)--(17, 0.1));
label("$\left(n+\frac52\right)\Delta t$", (17, -0.1), S);

// updates
arrowbar arr=Arrow(HookHead);
draw((2, 2){right}..(5, 0), arrow=arr);
draw((2, 0)..(5, -2){right}..(8, 0), arrow=arr);
label("$E^n\to E^{n+1}$", (5, -2), S);
draw((5, 0)..(8, 2){right}..(11, 0), arrow=arr);
label("$B^{n+\frac12}\to B^{n+\frac32}$", (8, 2), N);
draw((8, 0)..(11, -2){right}..(14, 0), arrow=arr);
label("$E^{n+1}\to E^{n+2}$", (11, -2), S);
draw((11, 0)..(14, 2){right}..(17, 0), arrow=arr);
label("$B^{n+\frac32}\to B^{n+\frac52}$", (14, 2), N);
draw((14, 0)..(17, -2){right}, arrow=arr);

