import common;

unitsize(0.9cm);

// PML regions fill
fill((10, 4)--(14.3, 4)--(14.3, 5)--(10, 5)--cycle, mediumgray);
fill((-0.3, 0)--(3, 0)--(3, 1)--(-0.3, 1)--cycle, mediumgray);

// coarse grid
draw((-0.3,4)--(14.3,4));
for (int i = 1; i <= 13; i += 3)
    bfieldarrow((i, 4));
for (int i = 2; i <= 11; i += 3)
    efieldcross((i + 0.5, 4));

// fine grid
draw((-0.3, 0)--(14.3, 0));
for (int i = 0; i <= 14; i += 1)
    bfieldarrow((i, 0));
for (int i = 0; i <= 13; i += 1)
    efieldcross((i+0.5, 0));

label("Крупная", (-0.5, 4), W);
label("Мелкая", (-0.5, 0), W);

// transfers
pen tr = dashdotted + linewidth(0.8pt);
arrowbar trarr = Arrow(HookHead, 7bp);
draw((4, 3.9)..(3.5, 0.1), arrow=trarr, p=tr);
draw((2.5, 3.9)..(3.9, 1.2), arrow=trarr, p=tr);
label("$\frac23$", (2.7, 3), W);
draw((5.5, 3.9)..(4.1, 1.2), arrow=trarr, p=tr);
label("$\frac13$", (5.3, 3), E);

tr = dashed + linewidth(0.8pt);
draw((7.1, 0.1)..(8.5, 3.9), p=tr, arrow=trarr);
draw((8.5, 0.1)..(7, 3.9), p=tr, arrow=trarr);

// transfer actors markings
label("$\left.E_y^c\right|_{i-\frac12}$", (2.5, 4.1), N);
label("$\left.B_z^c\right|_i$", (4, 5.1), N);
label("$\left.E_y^c\right|_{i+\frac12}$", (5.5, 4.1), N);
label("$\left.B_z^c\right|_{i+1}$", (7, 5.1), N);
label("$\left.E_y^c\right|_{i+\frac32}$", (8.5, 4.1), N);

label("$\left.E_y^f\right|_{j-\frac12}$", (3.5, -0.1), S);
label("$\left.B_z^f\right|_j$", (3.9, 0.7), E);
label("$\left.B_z^f\right|_{j+3}$", (7, -0.1), S);
label("$\left.E_y^f\right|_{j+\frac92}$", (8.5, -0.1), S);

