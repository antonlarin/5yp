unitsize(1cm);

// cube
draw((0, 0)--(4, 0)--(4, 4)--(0, 4)--cycle);
draw((0, 4)--(1, 5)--(5, 5)--(5, 1)--(4, 0));
draw((4, 4)--(5, 5));
draw((2, 0)--(2, 4)--(3, 5), p=dashed);
draw((0, 2)--(4, 2)--(5, 3), p=dashed);
draw((0.5, 4.5)--(4.5, 4.5)--(4.5, 0.5), p=dashed);

// axes
draw((-1, -1)--(-1, 0), arrow=Arrow(HookHead, 4bp));
draw((-1, -1)--(0, -1), arrow=Arrow(HookHead, 4bp));
draw((-1, -1)--(-1.7, -1.7), arrow=Arrow(HookHead, 4bp));
label("$z$", (-1, 0), NW);
label("$y$", (0, -1), SE);
label("$x$", (-1.7, -1.7), SW);

// arrows
// bx
pen arrowpen = linewidth(1bp);
arrowbar arr = Arrow(HookHead, 6bp);
dot((0.5, 4.5));
draw((0.5, 4.5)--(0.125, 4.125), p=arrowpen, arrow=arr);
label("$B_x$", (0.125, 4.125), NW);
dot((4.5, 4.5));
draw((4.5, 4.5)--(4.125, 4.125), p=arrowpen, arrow=arr);
dot((4.5, 0.5));
draw((4.5, 0.5)--(4.125, 0.125), p=arrowpen, arrow=arr);
label("$B_x$", (4.125, 0.125), SE);

// by
dot((2, 0));
draw((2, 0)--(3.5, 0), p=arrowpen, arrow=arr);
label("$B_y$", (2.75, 0), S);
dot((2, 4));
draw((2, 4)--(3.5, 4), p=arrowpen, arrow=arr);
label("$B_y$", (2.75, 4), S);
dot((3, 5));
draw((3, 5)--(4.5, 5), p=arrowpen, arrow=arr);
label("$B_y$", (4.5, 5), N);

// bz
dot((0, 2));
draw((0, 2)--(0, 3.5), p=arrowpen, arrow=arr);
label("$B_z$", (0, 2.75), W);
dot((4, 2));
draw((4, 2)--(4, 3.5), p=arrowpen, arrow=arr);
label("$B_z$", (4, 2.75), W);
dot((5, 3));
draw((5, 3)--(5, 4.5), p=arrowpen, arrow=arr);
label("$B_z$", (5, 3.75), E);

// ex
dot((2, 2));
draw((2, 2)--(0.94, 0.94), p=arrowpen, arrow=arr);
label("$E_x,\;j_x$", (1.4, 1.4), SE);

// ey
dot((4.5, 2.5));
draw((4.5, 2.5)--(6, 2.5), p=arrowpen, arrow=arr);
label("$E_y,\;j_y$", (6, 2.5), S);

// ez
dot((2.5, 4.5));
draw((2.5, 4.5)--(2.5, 6), p=arrowpen, arrow=arr);
label("$E_z,\;j_z$", (2.5, 6), E);

