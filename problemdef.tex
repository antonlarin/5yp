\section{Постановка задачи и цели работы}
\subsection{Основные понятия и обозначения}
\emph{Сетка} --- набор ячеек Йи (Kane Yee) (см. раздел~\ref{sec:pic}), организованный в виде параллелепипеда, покрывающий расчётную область или её часть.

\emph{Крупная сетка} --- сетка, которая покрывает всю расчётную область, за исключением подобласти, где необходимо более высокое разрешение.

\emph{Мелкая сетка} --- сетка, покрывающая подобласть расчётной области, где необходимо более высокое разрешение, чем может предоставить крупная сетка.

\emph{Интерфейс} --- граница подобласти, где необходимо повышенное разрешение, а также её окрестность. Поскольку данная работа сосредоточена на одномерных задачах, различаются \emph{правый} и \emph{левый} интерфейсы.

\emph{Связывание} --- группа операций, обеспечивающих перенос значений электромагнитного поля из крупной сетку в мелкую и наоборот. Первый тип связывания называется \emph{прямым}, второй --- \emph{обратным}.

\emph{Множитель разбиения} --- отношение некоторого шага в крупной сетке к его значению в мелкой сетке. Различаются \emph{множитель разбиения по пространству} --- \(p=\frac{\Delta x^c}{\Delta x^f}\), и \emph{множитель разбиения по времени} --- \(q=\frac{\Delta t^c}{\Delta t^f}\) (здесь и далее верхний индексы \(c\) и \(f\) обозначают величины, относящиеся к крупной и мелкой сеткам, соответственно). Если множители по времени и по пространству равны, то можно говорить просто о множителе разбиения. Также иногда применяется обозначение RF (от англ. refinement factor).

Метод конечных разностей FDTD оперирует электрическим \emph{полем} \(\vf{E}\) и магнитным --- \emph{полем} \(\vf{B}\). Значения компонент электрического и магнитного поля обозначаются, соответственно, \(\at{E_y^n}{i+\half}\) и \(\at{B_z^{n+\half}}{i}\), где \(E_y\) и \(B_z\) --- компоненты полей, \(n\) --- число выполненных итераций алгоритма, \(i\) --- индекс ячейки одномерной сетки, с которой ассоциированы данные компоненты полей, различия в индексах обусловлены особенностями алгоритма, осбсуждающимися далее.

\subsection{Общие сведения о методе частиц в ячейках}
\label{sec:pic}
Метод частиц в ячейках (particle-in-cell, PIC) \cite{birdsal-langdon} появился в 1950-х годах и в течение второй половины прошлого столетия за счёт развития компьютерных технологий и отдельных алгоритмов-составляющих метода стал одним из основных инструментов компьютерного моделирования плазмы. Он оперирует двумя сущностями --- электромагнитным полем, заданным и эволюционирующим на сетке, и ансамблем заряженных частиц. Поскольку в практических задачах число частиц должно быть очень велико (например, порядка \(10^9\)), вместо того, чтобы обрабатывать каждую частицу по отдельности, метод частиц в ячейках оперирует так называемыми макрочастицами --- сущностями, представляющими сразу несколько реальных частиц. Поскольку отношение общего заряда к массе остаётся таким же, как в случае работы с отдельными частицами, данная вычислительная техника не сказывается отрицательным образом на точности метода; макрочастицы двигаются похожим образом по сравнению с реальными частицами.

Вычислительный цикл метода состоит из четырёх этапов:
\begin{enumerate}
    \item Интегрирование уравнений динамики электромагнитного поля.
    \item Интерполяция значений полей в точки расположения частиц.
    \item Интегрирование уравнений движения частиц.
    \item Интерполяция зарядов и плотностей токов в точки сетки.
\end{enumerate}

На каждой из этих стадий можно применить различные численные методы. Подобная модифицируемость, безусловно, является одной из причин длительной актуальности метода. 

\subsubsection*{Интегрирование уравнений динамики электромагнитного поля}
Динамика электромагнитного поля описывается системой уравнений Максвелла. Ниже приведён их вид для случая вакуума в системе единиц СГС (сантиметр-грамм-секунда):

\begin{align}
    \nabla\times\vf{E}&=-\frac1c\frac{\partial \vf{B}}{\partial t}, \label{eq:faradays-law}\\
    \nabla\times\vf{B}&=\frac{4\pi\vf{j}}{c}+\frac1c\frac{\partial \vf{E}}{\partial t}, \label{eq:amperes-law}\\
    \nabla\cdot\mathbf{E}&=4\pi\rho, \label{eq:gauss-law} \\
    \nabla\cdot\mathbf{B}&=0. \label{eq:gauss-magnetic-law}
\end{align}

Для интегрирования этих уравнений могут применяться конечно-разностные, конечно-эле\-мен\-тны\-е, спектральные методы. Данная работа сосредоточена на использовании на этом этапе метода конечных разностей во временной области (finite-difference time-domain, FDTD).

\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\textwidth]{images/yee-cell.pdf}
    \caption{Ячейка сетки Йи}
    \label{fig:yee-cell}
\end{figure}

Метод FDTD был предложен в 1966 г. Кейном Йи (Kane Yee) \cite{yee} и является одним из наиболее широко используемых методов интегрирования уравнений Максвелла благодаря надёжности и простоте реализации \cite{taflove}. В его основе лежит сетка Йи, ячейка которой изображена на рис.~\ref{fig:yee-cell}.

Сетка Йи состоит из 6 сеток (по одной на каждую компоненту электромагнитного поля), сдвинутых на половину шага по одной или двум осям относительно основной сетки (определяющей границ ячеек). Особая структура сетки Йи позволяет дискретизировать уравнения Максвелла, заменив каждую пространственную производную на центральный разностный оператор и, таким образом, гарантировать второй порядок сходимости по пространственным переменным. Кроме этого, метод FDTD использует попеременную схему обновления во времени (leapfrog integration, проиллюстрирована на рис.~\ref{fig:fdtd-leapfrog}), которая обеспечивает второй порядок сходимости метода по переменной \(t\).

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{images/fdtd-leapfrog.pdf}
    \caption{Попеременная схема обновления полей в методе FDTD}
    \label{fig:fdtd-leapfrog}
\end{figure}

\subsubsection*{Интерполяция полей}
Чтобы иметь возможность проинтегрировать уравнения движения частиц, необходимы значения полей \(\vf{E}\) и \(\vf{B}\) в точках, где находятся частицы. При этом значения полей известны только в узлах сетки Йи, в то время, как частицы могут находиться в любой точке пространства. Интерполяция значений полей с сетки в положения частиц может осуществляться полиномиальными методами, либо за счёт введения форм-фактора --- функции, которая описывает форму облака частиц, аппроксимируемого данной макрочастицей. К числу часто используемых форм-факторов относятся NGP (nearest grid point, соответствует интерполяции 0 порядка), CIC (cloud in cell, соответствует линейной интерполяции) \cite[стр.~24]{birdsal-langdon}, TSC (triangular shaped cloud).

\subsubsection*{Интегрирование уравнений движения частиц}
На данном этапе на основе текущих позиций и импульсов макрочастиц, а также интерполированных значений полей рассчитываются новые позиции и импульсы. Движение частиц описывается вторым законом Ньютона с силой Лоренца:

\begin{align}
\frac{\partial \vf{p}_i}{\partial t}&=q_i\left(\vf{E}_i+\frac1c\vf{v}_i\times\vf{B}_i\right) \\
\frac{\partial \vf{x}_i}{\partial t}&=\frac{\vf{p}_i}{m_i}\left(1+\left(\frac{\vf{p}_i}{m_i c}\right)^2\right)^{-\frac12}
\end{align}

Де-факто стандартным методом движения частиц является метод Бориса \cite{boris}, в котором изменение импульса частицы производится в виде особой последовательности преобразований, отдельно учитывающей ускорение, придаваемое ей электрическим и магнитным полями. Его незаменимость в современных PIC-кодах обуславливается тем, что он сохраняет высокий уровень точности на протяжении большого количества итераций благодаря тому, что оператор перехода метода сохраняет объём в фазовом пространстве \cite{qin-zhang-xiao-liu-sun-tang}.

\subsubsection*{Взвешивание токов}
На данном этапе вычисляются плотности токов, создаваемые движением частиц, и они распределяются по точкам сетки для последующего использования в интегрировании полей (ур.~\ref{eq:amperes-law} содержит слагаемое с плотностью тока). Для определения вкладов частиц в отдельные узлы сетки могут использоваться как ранее упомянутые форм-факторы, так и более сложные схемы, ориентированные на высокую точность, или выполнение законов сохранения: методы Т. Ж. Есиркепова \cite{esirkepov}, Вилласенора-Бунемана \cite{villasenor-buneman}, Зигзаг \cite{zigzag1, zigzag2}.

\subsection{Использование подсеток в методе частиц в ячейках}
Использование неравномерных сеток является классическим и широко исследованным подходом для локального повышения разрешения в различных классах задач. Основная польза от использования неравномерных сеток заключается в уменьшении вычислительной трудоёмкости моделирования. Для примера, метод FDTD использует регулярную сетку и, будучи явным методом, имеет ограничение на диапазон параметров, при котором обеспечивается устойчивость (критерий Куранта):

\begin{equation}
\Delta t\leqslant\frac{1}{c\sqrt{\frac{1}{\Delta x^2}+\frac{1}{\Delta y^2}+\frac{1}{\Delta z^2}}}
\end{equation}

Если необходимо увеличить пространственное разрешение в \(\alpha\) раз, то в случае уменьшения шага во всей сетке число ячеек увеличивается в \(\alpha^3\) раз. Для обеспечения устойчивости метода нужно также уменьшить в \(\alpha\) раз шаг по времени. Это означает, что общие затраты на интегрирование уравнений Максвелла возрастают в \(\alpha^4\) раз. Если повышенное разрешение необходимо лишь в небольшой части расчётной области, использование неравномерных сеток кажется привлекательным решением.

За последние 30 лет использование неравномерных сеток в методе FDTD было достаточно глубоко изучено. Были сформулированы основные проблемы, с которыми должны бороться предлагаемые методы. Наиболее важными из этих проблем являются отражения от границ мелкой сетки и поздняя неустойчивость (нефизичный экспоненциальный рост энергии поля). Однако, несмотря на большое количество предложенных методов, среди них нет общепринятого и широко используемого (как, например, метод Бориса движения частиц).

Реализация метода использования неравномерных сеток в методе частиц в ячейках --- несколько другая задача. Даже если для интегрирования электромагнитного поля в PIC используется метод FDTD, для успешного использования алгоритмов работы с неравномерными сетками их нужно расширить, включив операции с частицами. Реализации этих операций должны быть выполнены аккуратно, потому что на интерфейсе могут нарушаться законы сохранения и появляться нефизичные эффекты, такие, как оказание частицей воздействия на саму себя и появление ложных зарядов, создающих статическое электрическое поле. Кроме этого, воздействие частиц создаёт ещё более неблагоприятную ситуацию для алгоритма использования неравномерной сетки, чем просто электродинамическая задача, что задаёт более жёсткие требования к демонстрируемым отражениям и устойчивости схемы.

Эта задача не была исследована настолько широко, как задача для FDTD. Одним из основных авторов публикаций по данной тематике является J.-L. Vay. В \cite{vay-2002} он и группа коллег обозначили 2 подхода к применению неравномерных сеток в PIC-моделировании. В первом подходе задача сначала решается полностью на крупной сетке, после чего из крупной сетки выделяются значения полей на границе области, где необходимо повышенное разрешение. Эти значения используются как граничные условия для другого расчёта, который проводится на мелкой сетке, имеющей размер области повышенного разрешения. Поскольку на мелкой сетке узлы расположены чаще, чем на крупной, граничные условия для мелкой сетки нельзя получить лишь из узлов крупной сетки, недостающие значения предлагается интерполировать. Во втором подходе расчёт переключается между крупной и мелкой сетками: производится одна итерация метода на крупной сетке, с крупной сетки интерполируются граничные условия для мелкой сетки, на мелкой сетке проводится несколько итераций расчёта, значения, полученные на мелкой сетке синхронизируются со значениями на крупной сетке (в настоящей работе рассматриваются схемы, вписывающиеся именно в этот подход). В \cite{vay-2004} была предложена схема, основанная на первом подходе, и нацеленная на борьбу с многократными отражениями высокочастотных компонент в области с повышенным разрешением. В данной схеме кроме крупной и мелкой есть ещё одна сетка, размер ячейки которой совпадает с размером ячейки в крупной сетке, но сама она имеет размер области с повышенным разрешением. Идея метода заключается в том, что эта вспомогательная сетка должна разрешать частоты, распространяющиеся в крупной сетке, а затем значения полей, полученные на ней, могут быть использованы для того, чтобы извлечь из мелкой сетки лишь высокочастотные компоненты. Обе меньшие сетки обрамлены слоями поглощающих граничных условий.

Другой подход был предложен K. Fujimoto в \cite{fujimoto}. Вместо целых сеток автор предлагает оперировать отдельными ячейками. Каждая ячейка может быть подразбита на \(2^d\) подъячейки, где \(d\) --- размерность пространства задачи. Благодаря такому подходу, достигается более высокий уровень гибкости в определении области с повышенным разрешением, появляется возможность эффективно повышать разрешение в областях со сложной геометрией. Ячейки одного размера объединяются в уровни, на каждом из которых действует свой временной шаг, причём, чем меньше размер ячейки, тем выше считается уровень. Связывание также действует в терминах уровней. При переходе на уровень выше происходит копирование значений полей из более низких уровней с интерполяцией, при переходе на уровень ниже --- усреднение. Интерполяция полей в позицию частицы и взвешивание токов производятся на уровне с наиболее мелким шагом, из тех, что есть в данной точке. На более низких уровнях значения плотности тока также заимствуются с верхних уровней путём усреднения. Для борьбы с поздней неустойчивостью в методе используется сглаживание --- периодическое взвешенное усреднение всех компонент полей по \(3^d\) точкам.

Ещё одна схема, опробованная в рамках метода PIC, была предложена F. Teixeira и B. Donderici. В работах \cite{donderici-teixeira-2005, donderici-teixeira-2006} они предложили схему для использовании в методе FDTD, а в диссертации B. Donderici \cite{donderici-phd} было рассмотрено применение этой схемы в методе частиц в ячейках. Прототип этой схемы был реализован в рамках данной работы, более подробно ее описание приведено в разделе~\ref{sec:scheme3}. Отметим лишь, что авторы заявляют о хорошей точности своего алгоритма, что позволяет его использовать в методе частиц в ячейках, применив некоторую модификацию для борьбы с мнимыми зарядами.

\subsection{Цель и задачи работы}
Целью данной работы является создание эффективной реализации метода использования неравномерных сеток в PIC-коде PICADOR. Для достижения этой цели необходимо решить следующие задачи:

\begin{enumerate}
    \item Анализ источников на предмет существующих решений данной задачи.
    \item Реализация прототипов выбранных численных схем и исследование их характеристик и применимости.
    \item Реализация наилучшей по результатам испытаний схемы в коде PICADOR.
    \item Проведение экспериментов со схемой с целью установления её корректности.
    \item Исследование производительности реализации и её оптимизация.
\end{enumerate}

